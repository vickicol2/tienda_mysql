show databases;
create database mitienda;
use mitienda;

create table producto(
codigo varchar(10) not null,
nombre varchar(15) not null,
precio float(4,2) unsigned not null,
fecha date not null,
primary key (codigo)
);

show tables;
describe producto;

alter table producto modify nombre varchar(30) not null;
describe producto;

insert into producto values ('c01','Cepillo abrillantar', 2.50, '2017-11-02');
insert into producto values ('r01','Regleta mod. ZAZ', 10, '2018-05-03');
insert into producto values ('r02','Regleta mod. XAX', 15, '2018-05-03');
insert into producto values ('p01','Pegamento rápido', 6.50, '2017-11-02');
insert into producto values ('p02','Pegamento industrial', 14.50, '2017-10-06');
insert into producto values ('a03','Azada grande 60cm', 50.60, '2018-05-03');
insert into producto values ('c02','Cepillo birutas', 6.20, '2017-10-06');
insert into producto values ('c03','Cepillo jardín', 22.35, '2018-05-03');
insert into producto values ('a01','Azada pequeña 30cm', 25, '2017-10-06');
insert into producto values  ('a02','Azada mediana 45 cm', 37.50, '2017-11-02');

-- Mostrar todos los datos introducidos
select * from producto;

-- Mostrar los productos que tienen el nombre Azada
select * from producto where nombre like 'Azada%';

-- Ver sólo el nombre y el precio de los que tienen el precio mayor que 22
select nombre,precio from producto where precio>22;

-- Ver el precio medio de aquellos productos cuyo nombre comienza con "regleta
select avg(precio) as PrecioMedio from producto where nombre like 'regleta%';

-- Modificar la estructura de la tabla para añadir un nuevo campo: "categoría"
alter table producto add column categoria varchar(20) not null;
describe producto;

-- Dar el valor "utensilio" a la categoría de todos los productos existentes
update producto set categoria='utensilio';
select * from producto;

-- Modificar los productos que comienza por la palabra "regleta", para que su categoría sea "enchufes".
update producto set categoria='enchufes' where nombre like 'regleta%';

 -- Ver la lista categorías
select categoria from producto;

-- Ver la cantidad de productos que tenemos en cada categoría.
select count(categoria) from producto group by (categoria);

-- Introducir a fecha actual, un nuevo utensilio con nombre Cepillo Birutas pequeño, con código “c02”, y con precio 6.30.
insert into producto (fecha,categoria,nombre,codigo,precio) values (now(),'utensilio','Cepillo Birutas pequeño', 'c02', 6.30);

-- Renombrar la tabla con el nombre ‘Productos B’
alter table producto rename to productoB;

show tables;
